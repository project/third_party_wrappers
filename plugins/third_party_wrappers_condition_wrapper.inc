<?php

/**
 * Context condition for if the page is being output as a third party wrapper.
 */
class third_party_wrappers_condition_wrapper extends context_condition {
  /**
   * Override values for the context settings form.
   */
  function condition_values() {
    return array(
      'all' => t('All (Top and Bottom simultaneously)'),
      'top' => t('Top'),
      'bottom' => t('Bottom'),
    );
  }

  /**
   * Execute the context.
   */
  function execute() {
    if (isset($_GET['third_party_wrappers'])) {
      $action = $_GET['third_party_wrappers'];
      foreach ($this->get_contexts() as $context) {
        $values = $this->fetch_from_context($context, 'values');
        if (array_key_exists($action, $values)) {
          $this->condition_met($context);
        }
      }
    }
  }
}
